// Library Imports
import React from 'react';

import {blogPosts} from '../words.js';
import BlogPage from '../../components/BlogPage';

const blogProps = blogPosts['2023-03-01'];
export default function BlogPost() {
    return (
    <BlogPage {...blogProps}>
        <p>
            One of the most helpful post-it notes I ever placed in my office at work was how to write ternaries in Python and JavaScript.
            I would find myself looking up the syntax over and over again, until finally I decided to write it down and stick it to the bottom of my monitor.
        </p>
        <div>
            <h5>Python:</h5>
            <pre>
                <code className='language-python'>
                result = option1 if condition else option2
                </code>
            </pre>

            <h5>JavaScript:</h5>
            <pre>
                <code className='language-javascript'>
                const result = condition ? option1 : option2;
                </code>
            </pre>
        </div>
        <br />
        <p>
            (Code syntax highlighting by <a href='https://prismjs.com/' target='_blank'  rel='noreferrer'>Prism</a>)
        </p>
    </BlogPage>
    );
};
