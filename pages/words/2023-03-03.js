// Library Imports
import React from 'react';

import {blogPosts} from '../words.js';
import BlogPage from '../../components/BlogPage';

const blogProps = blogPosts['2023-03-03'];
export default function BlogPost() {
    return (
    <BlogPage {...blogProps}>
        <p>
            Although I am shocked to find there is no Bloody Mary emoji, I am delighted to share a quick recipe for my new favorite drink. Perfect for Brunch, Dinner, or that late-night coding session.
        </p>
        <ul>
            <li>Low-ball glass (idk why but I like it better in a shorter glass)</li>
            <li>2-3 ice cubes</li>
            <li>4 oz of bloody mary mix</li>
            <li>1.5 oz vodka</li>
            <li>1 dash of fresh-ground black pepper</li>
            <li>Stir in a celery spear or two</li>
            <li>Garnish with a pickle</li>
        </ul>
        {/* TODO: Get a nice pic of a Bloody Mary */}
        <p>
            <strong>Disclaimer:</strong> Everything in moderation folks. Alcohol is bad for you. 
        </p>
    </BlogPage>
    );
};
