// Library Imports
import React from 'react';

import {blogPosts} from '../words.js';
import BlogPage from '../../components/BlogPage';

const blogProps = blogPosts['2023-02-19'];
export default function BlogPost() {
  return (
    <BlogPage {...blogProps}>
      <p>
        I am mostly intested in experimenting with Next.js but this is a great excuse to get some thoughts down on paper.
        You can expect topics such as programming, web-programming, video games, indie-games, music, movies, and much more.
      </p>
    </BlogPage>
  );
};
