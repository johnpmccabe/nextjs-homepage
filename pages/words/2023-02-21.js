// Library Imports
import React from 'react';

import {blogPosts} from '../words.js';
import BlogPage from '../../components/BlogPage';

const blogProps = blogPosts['2023-02-21'];
export default function BlogPost() {
  return (
    <BlogPage {...blogProps}>
      <div>
        Lofi Girl<br/>
        <iframe width="560" height="315" src="https://www.youtube.com/embed/jfKfPfyJRdk" title="YouTube video player" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowFullScreen></iframe>
        <br/>
        Miss Monique<br/>
        <iframe width="560" height="315" src="https://www.youtube.com/embed/yoRySSq634o" title="YouTube video player" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowFullScreen></iframe>
      </div>
    </BlogPage>
  );
};
