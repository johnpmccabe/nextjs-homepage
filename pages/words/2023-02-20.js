// Library Imports
import React from 'react';

import {blogPosts} from '../words.js';
import BlogPage from '../../components/BlogPage';

const blogProps = blogPosts['2023-02-20'];
export default function BlogPost() {
  return (
    <BlogPage {...blogProps}>
      <ul>
        <li>Starve The Ego, Feed The Soul - The Glitch Mob</li>
        <li>Your Hand In Mine - Explosions In The Sky</li>
        <li>Where Is My Mind? (The Pixies cover) - Sunday Girl</li>
        <li>Kusanagi - ODESZA</li>
        <li>Staring at the Sun - TV on the Radio</li>
        <li>Laktos - Sebastian Ingrosso</li>
        <li>In Motion - Trent Reznor & Atticus Ross	The Social Network (Soundtrack from the Motion Picture)	Soundtrack</li>
        <li>The Monk - Joris Voorn</li>
        <li>Too Long / Steam Machine - Daft Punk	Alive 2007 (Live)</li>
        <li>Flight - Tristam & Braken	Monstercat (3 Year Anniversary)</li>
      </ul>
    </BlogPage>
  );
};
