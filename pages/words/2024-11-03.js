// Library Imports
import React from 'react';

import {blogPosts} from '../words.js';
import BlogPage from '../../components/BlogPage';

const blogProps = blogPosts['2024-11-03'];
export default function BlogPost() {
    return (
    <BlogPage {...blogProps}>
        <p>
            Howdy <strong>folks</strong>, just checking my CI still works.
        </p>
    </BlogPage>
    );
};
