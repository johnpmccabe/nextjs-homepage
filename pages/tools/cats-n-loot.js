// Library Imports
import Head from 'next/head';
// Project Imports
import styles from '../../styles/Home.module.css';
import Layout from '../../components/layout';
import ToolPage from '../../components/ToolPage';
import {ToolDict} from '../index.js';

const toolProps = ToolDict['cats-n-loot'];
export default function CatsNLoot() {
  return (
    <Layout className={styles.container}>
      <Head>
        <title>John McCabe - {toolProps.name}</title>
      </Head>
      <ToolPage {...toolProps}>
        <div className={styles.extras}>
          <h3>About:</h3>
          <p>
            Cats & Loot started out as just a few whiteboard sketches after a particularly hectic year(2016?) of social media.
            I dreamed of a feed with nothing but soothing cat content.
            No bad news, no slimy scams, no distractions, just feline friends.
          </p>
          <p>
            In this game you scroll through posts and heart (❤️) the cats because you love them, and snooze (💤) anything work related.
            Earn points by just loving cats.
            You can even spend your points on upgrades!
          </p>
          <h3>Cat Credits:</h3>
          <ul>
            <li>Alice</li>
            <li>Ivan & Otto</li>
            <li>Martok (Marty)</li>
            {/* <li>Mocha</li> */}
            {/* <li>Nino</li> */}
            <li>and several cats from <a href='https://unsplash.com'>Unsplash.com</a></li>
          </ul>
        </div>
      </ToolPage>
    </Layout>
  );
};
