// Library Imports
import Head from 'next/head';
// Project Imports
import styles from '../../styles/Home.module.css';
import Layout from '../../components/layout';
import ToolPage from '../../components/ToolPage';
import {ToolDict} from '../index.js';

const toolProps = ToolDict['cash-n-fruit'];
export default function CashNFruit() {
  return (
    <Layout className={styles.container}>
      <Head>
        <title>John McCabe - {toolProps.name}</title>
      </Head>
      <ToolPage {...toolProps}>
      <div className={styles.extras}>
        <h3>Credits:</h3>
        <p>
          Featuring Music by <a href='https://www.shaunwhitley.com/'>Shaun Whitley</a>
        </p>
        <p>
          Several CSS Animations from <a href='https://animista.net/'>https://animista.net/</a>
        </p>
        <h3>About:</h3>
        <p>
          Cash &apos;n Fruit was one of my earlier experiments at building a web game using the <a href='https://create-react-app.dev'>create-react-app</a>.
        </p>
        <p>
          I started development by hacking on a game board of clickable buttons.
          Then I set out to find the perfect Emojis to use as icons. (No custom assets for this one)
          Once I picked a couple of cool fruit and the flying cash, we were <i>off-to-the-races</i>.
        </p>
        <p className={styles.bigEmoji}>💸🍓🍏🍊💣🔮</p>
        <p>
          Emojis are also used to control in-game Music (🔈🔇), Animations (💃🕴), and SFX (🔔🔕) settings.
        </p>
        <p>
          I had a lot of fun implementing the A* algorithim to find neighboring buttons of the same type.
          Connecting more fruit/cash in your combo means more points!
        </p>
        <p>
          This game is still a work-in-progress.
        </p>
      </div>
      </ToolPage>
    </Layout>
  );
};
