// Library Imports
import Head from 'next/head';
// Project Imports
import styles from '../../styles/Home.module.css';
import Layout from '../../components/layout';
import ToolPage from '../../components/ToolPage';
import {ToolDict} from '../index.js';

const toolProps = ToolDict['namegen'];
export default function NameGen() {
  return (
    <Layout className={styles.container}>
      <Head>
        <title>John McCabe - {toolProps.name}</title>
      </Head>
      <ToolPage {...toolProps} />
    </Layout>
  );
};
