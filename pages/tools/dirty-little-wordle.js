// Library Imports
import Head from 'next/head';
// Project Imports
import styles from '../../styles/Home.module.css';
import Layout from '../../components/layout';
import ToolPage from '../../components/ToolPage';
import {ToolDict} from '../index.js';

const toolProps = ToolDict['dirty-little-wordle'];
export default function DirtyLittleWordle() {
  return (
    <Layout className={styles.container}>
      <Head>
        <title>John McCabe - {toolProps.name}</title>
      </Head>
      <ToolPage {...toolProps}>
        <div className={styles.extras}>
          <h3>Disclaimer:</h3>
          <p>
            This game contains lots of NSFW (not safe for work) language.
            And that is entirely the point.
            The Wordle solution was never going to be BUTTS.
            Now it can be.
          </p>
        </div>
      </ToolPage>
    </Layout>
  );
};
