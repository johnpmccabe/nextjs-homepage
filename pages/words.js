// words.js is the main blog page
// Library Imports
import Head from 'next/head';
// import Image from 'next/image';
import Link from 'next/link';
// Project Imports
import styles from '../styles/Home.module.css';
import Layout from '../components/layout';
import {Image} from '../components/ImageLoader';

export const blogPosts = {
  // { // Template
  //   slug: 'YYYY-MM-DD',
  //   date: 'Mar. 1st 2023',
  //   title: 'blah blah',
  //   content: 'Blah blah blah',
  // },
  '2023-03-03': {
    slug: '2023-03-03',
    date: 'Mar. 3rd 2023',
    title: 'Simple Bloody Mary 🥫🍸',
    content: 'TL;DR - Tomato juice and vodka is actually pretty tasty.',
  },
  '2023-03-01': { // TODO: Soon...
    slug: '2023-03-01',
    date: 'Mar. 1st 2023',
    title: 'Ternary Operators for Fun and Profit ☘️',
    content: 'Whether you are building dynamic UIs or complex data pipelines, ternary operators are an essential tool in the toolbox of any developer.',
  },
  '2023-02-21': {
    slug: '2023-02-21',
    date: 'Feb. 21st 2023',
    title: 'YouTube Gems 💎',
    content: 'A few of my favorite YouTube channels.',
  },
  '2023-02-20': {
    slug: '2023-02-20',
    date: 'Feb. 20th 2023',
    title: 'Programming Playlist 🎧',
    content: 'Music to help get into a programming flow state. Genres include electronic, post-rock, and dance.',
  },
  '2023-02-19': {
    slug: '2023-02-19', // todo: sluggies
    date: 'Feb. 19th 2023',
    title: 'Hello World 👋',
    content: 'Welcome to my (Next.js powered) blog. Hopefully, by the time you are reading this I will have posted something more interesting than just "Hello".',
  },
};

export default function Home() {
  return (
    <Layout className={styles.container} homepage={false}>
      <Head>
        <title>John McCabe - Words</title>
        <meta name="description" content="Homepage for John McCabe" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className={styles.grid}>
        {Object.keys(blogPosts).map((x, count) => (
          <Link
            // href={`/words/${blogPosts[x].slug}`}
            href={`/words/${x}`}
            key={`blog${count}`}
          >
            <a className={styles.blogCard} tabIndex='0'>
              <div>
                <h1>{blogPosts[x].title}</h1>
                <h5>{blogPosts[x].date}</h5>
                  <p className={styles.wordContent}>{blogPosts[x].content}</p>
              </div>
            </a>
          </Link>
        ))}
      </div>
      {/* 
      <div className={styles.grid}>
        { Object.keys(ToolDict).map(x => (
            <Link key={`link${x}`} href={ToolDict[x].path}>
              <a className={styles.card} style={{
                // background: `linear-gradient(215deg, #41e7fc, #000000), linear-gradient(132deg, ${ToolDict[x].color}, #000000)`,
              }}>
                <h2>{ToolDict[x].title} &rarr;</h2>
                <Image
                  alt={`Screenshot of ${ToolDict[x].title}`}
                  src={`images/${x}.png`}
                  width={2528}
                  height={1630}
                />
              </a>
            </Link>
          ))}
      </div>
     */}
    </Layout>
  );
};
