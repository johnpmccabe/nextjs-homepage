// Next.js Homepage
// Library Imports
import Head from 'next/head';
// import Image from 'next/image';
import Link from 'next/link';
// Project Imports
import styles from '../styles/Home.module.css';
import Layout from '../components/layout';
import {Image} from '../components/ImageLoader';


export const ToolDict = {
  'cash-n-fruit': {
    name: 'cash-n-fruit',
    title: 'Cash \'n Fruit',
    path: '/tools/cash-n-fruit',
    description: 'Tile-based puzzle game featuring cash and fruit.',
    // TODO: Still private
    // sourceCode: 'https://gitlab.com/johnpmccabe/cash-n-fruit',
    launchURL: 'https://cash-n-fruit.now.sh/',
  },
  'cats-n-loot': {
    name: 'cats-n-loot',
    title: 'Cats & Loot',
    path: '/tools/cats-n-loot',
    description: 'A game about Cats! and social media.',
    // sourceCode: 'https://gitlab.com/johnpmccabe/cats-n-loot',
    launchURL: 'https://cats-n-loot.vercel.app/',
  },
  'chill': {
    name: 'chill',
    title: 'Chill',
    path: '/tools/chill',
    description: 'Generate linear-gradients and background animations.',
    sourceCode: 'https://gitlab.com/johnpmccabe/chill',
    launchURL: 'https://chill-sigma.vercel.app',
  },
  'namegen': {
    name: 'namegen',
    title: 'NameGen',
    path: '/tools/namegen',
    description: 'A name generator tool to combine and scramble words.',
    sourceCode: 'https://gitlab.com/johnpmccabe/namegen',
    launchURL: 'https://namegen-beta.vercel.app',
  },
  'dirty-little-wordle': {
    name: 'dirty-little-wordle',
    title: 'Dirty Little Wordle',
    path: '/tools/dirty-little-wordle',
    description: 'Like wordle, except all the answers are NSFW.',
    sourceCode: 'https://gitlab.com/johnpmccabe/dirty-little-wordle',
    launchURL: 'https://dirty-little-wordle.vercel.app/',
  },
};

export default function Home() {
  return (
    <Layout className={styles.container} homepage={true} >
      <Head>
        <title>John McCabe - Home</title>
        <meta name="description" content="Homepage for John McCabe" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className={styles.grid} role='list'>
        { Object.keys(ToolDict).map(x => (
            <Link key={`link${x}`} href={ToolDict[x].path}>
              <a className={styles.card}
                  tabIndex="0"
                  onKeyUp={(e) => {
                    return (e.code === 'Space' || e.code === 'Enter') ? router.push(ToolDict[x].path) : null
                  }}
                >
                  <div>
                <h2>{ToolDict[x].title} &rarr;</h2>
                {/* <p>{ToolDict[x].description}</p> */}
                <Image
                  alt={`Screenshot of ${ToolDict[x].title}`}
                  src={`images/${x}.png`}
                  width={2528}
                  height={1630}
                />
                </div>
              </a>
            </Link>
          ))}
      </div>
    </Layout>
  );
};
