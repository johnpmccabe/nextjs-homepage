# NextJS-Homepage

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

The goal is to deploy a static site to GitLab pages automatically.

Deploying to Vercel was very fast/easy but this was always more of a GitLab pages project than a Vercel (or Next.js) project.

[View on GitLab Pages](https://johnpmccabe.gitlab.io/nextjs-homepage/)


## Getting Started with Local Development

1. First, clone to repo.

1. Next, install dependencies with:

	```bash
	yarn
	```

1. Then, run the development server:

	```bash
	yarn dev
	```
