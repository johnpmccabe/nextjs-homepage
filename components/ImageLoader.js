// fancy gitlab pages compatible Image stuff
import NextImage from 'next/image';

// image loader
export const myLoader = ({ src, width, quality }) => {
  return `https://johnpmccabe.gitlab.io/nextjs-homepage/${src}?w=${width}&q=${quality || 75}`
};

const myDoNothingLoader = ({ src }) => {
  // return src
  return `https://johnpmccabe.gitlab.io/nextjs-homepage/${src}`;
};

export const Image = props => {
  return (
    <NextImage {...props} loader={myDoNothingLoader} />
  );
};
