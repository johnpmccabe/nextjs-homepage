// Library Imports
import { useEffect } from 'react';
import Head from 'next/head';
import Link from 'next/link';
import { useRouter } from 'next/router'
// import Image from 'next/image';
import Prism from "prismjs";


// import 'prismjs/plugins/toolbar/prism-toolbar.js';
// import 'prismjs/plugins/copy-to-clipboard/prism-copy-to-clipboard.js';
// import 'prismjs/plugins/line-numbers/prism-line-numbers.js';
// import 'prismjs/plugins/line-numbers/prism-line-numbers.css';
// import 'prismjs/plugins/unescaped-markup/prism-unescaped-markup.min.js';

import styles from '../styles/Home.module.css';

export default function Layout({children, homepage, words, ...props}) {
  useEffect(() => {
    Prism.manual = true;
    Prism.highlightAll();
    console.log('highlighted', Prism)
  }, []);
  const router = useRouter();
  const backLink = words ? '/words' : '/';
  return (
    <>
      <Head>
        <meta name="viewport"
          // content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"
          content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' 
        />
      </Head>
      <main className={styles.main} role='main'>
        <h1 className={styles.title}>
          John <Link href='/'><a>McCabe</a></Link>
          {/* <a href='https://twitter.com/johnpmccabe'>🐦</a> */}
        </h1>
        { homepage &&
            <div className={styles.description}>
              <p>Games, Tools, and <Link href='/words'><a tabIndex="0"
                onKeyUp={(e) => {
                  return (e.code === 'Space' || e.code === 'Enter') ? router.push('/words') : null
                }}>Words</a></Link> on the Web</p>
              <a href="https://twitter.com/johnpmccabe?ref_src=twsrc%5Etfw" data-show-count="false">@johnpmccabe</a>
            </div>
        }

        <div className={styles.container}>
          {children}
        </div>
        { !homepage &&
          <div className={styles.grid}>
            <Link href={backLink} >
              <div className={styles.backButton}
                tabIndex="0"
                onKeyUp={(e) => {
                  return (e.code === 'Space' || e.code === 'Enter') ? router.push(backLink) : null
                }}
              >
                <a>&larr; Back</a>
              </div>
            </Link>
          </div>
        }
      </main>
      <footer className={styles.footer}>
        {/*
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <span className={styles.logo}>
            <Image src="/vercel.svg" alt="Vercel Logo" width={72} height={16} />
          </span>
        </a>
        */}
        <i>Hack the Planet!</i>        
      </footer>
    </>
  )
};
