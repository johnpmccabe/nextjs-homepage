// Library Imports
import Head from 'next/head';
// import Image from 'next/image';
import Link from 'next/link';
import dynamic from 'next/dynamic';
// Project Imports
import styles from '../styles/Home.module.css';
import Layout from '../components/layout';
import {Image} from '../components/ImageLoader';


const myLoader = ({ src, width, quality }) => {
  return `https://johnpmccabe.gitlab.io/nextjs-homepage/${src}?w=${width}&q=${quality || 75}`
}

export default function BlogPage({ title, slug, date, content, page, extraPage, children, ...restOfThoseProps }) {
    return (
      <Layout className={styles.container} homepage={false} words={true}>
        <Head>
          <title>John McCabe - {title}</title>
          <meta name="description" content="Homepage for John McCabe" />
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <div className={styles.grid}>
          <main className={styles.blogPage}>
            <h1>{title}</h1>
            <h5 className='byline'>
              John McCabe -&nbsp;
              <time dateTime={{slug}}>
                {date}
              </time>
            </h5>
            <p>{content}</p>
            {/* <div className={styles.blogPageExtras} dangerouslySetInnerHTML={{ __html: page}} /> */}
            <div className={styles.blogPageExtras}>
              {/* {extraPage in Extras ? React.createElement(Extras[extraPage]) : null} */}
              {children}
            </div>
          </main>
        </div>
      </Layout>
    );
  };