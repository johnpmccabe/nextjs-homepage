// Library Imports
import Head from 'next/head';
// import Image from 'next/image';
// Project Imports
import {Image} from './ImageLoader';
import styles from '../styles/ToolPage.module.css';


const myLoader = ({ src, width, quality }) => {
  return `https://johnpmccabe.gitlab.io/nextjs-homepage/${src}?w=${width}&q=${quality || 75}`
}

export default function ToolPage({children, ...props}) {
  return (
    <div className={styles.toolCard}>
      <h2 className={styles.toolTitle}>{props.title}</h2>
      <p>{props.description}</p>
      <Image
        // loader={myLoader}
        alt={`Screenshot of ${props.name}`}
        src={`images/${props.name}.png`}
        width={2528}
        height={1630}
      />
      <div className={styles.grid}>
        { props.launchURL && 
          <a href={props.launchURL} target='_blank' rel='noreferrer'>
            <div className={styles.card}>
              <p>Launch 🚀</p>
            </div>
          </a>
        }
        { props.sourceCode && 
          <a href={props.sourceCode} target='_blank' rel='noreferrer'>
            <div className={styles.card}>
              <p>Source 🏗️</p>
            </div>
          </a>
        }
      </div>
      {children}
    </div>
  );
};
