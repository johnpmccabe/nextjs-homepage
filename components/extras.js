// Library Imports
import Head from 'next/head';
// import Image from 'next/image';
import Link from 'next/link';
import dynamic from 'next/dynamic';
// Project Imports
import styles from '../styles/Home.module.css';
import Layout from './layout';
import {Image} from './ImageLoader';

const TBD = () => {
  return (
    <div className={styles.tbd}>
      <div className={styles.tbd2}></div>
    </div>
  )
}

const Hello = (props) => {  
  return (
    <div>
      In Python:
      <pre>
        <code className='language-python'>
          result = option1 if condition else option2
        </code>
      </pre>

      In JavaScript:
      <pre>
        <code className='language-javascript'>
          const result = condition ? option1 : option2;
        </code>
      </pre>

    </div>
  );
};

export default () => ({
  ternary: Hello,
  tbd: TBD,
});
