/** @type {import('next').NextConfig} */
const nextConfig = {
  // i18n: {
  //   locales: ['en'],
  //   defaultLocale: 'en',
  // },
  reactStrictMode: true,
  trailingSlash: true,
  basePath: process.env.NODE_ENV === 'production' ? '/nextjs-homepage' : '',
  assetPrefix: process.env.NODE_ENV === 'production' ? '/nextjs-homepage' : '/',
  // assetPrefix: '/nextjs-homepage', // idk
  images: {
    loader: "custom",
  },
  exportPathMap: async (defaultPathMap,
    { dev, dir, outDir, distDir, buildId }) => ({
      '/': { page: '/' },
      '/tools/cash-n-fruit': { page: '/tools/cash-n-fruit', },
      '/tools/cats-n-loot': { page: '/tools/cats-n-loot', },
      '/tools/chill': { page: '/tools/chill', },
      '/tools/dirty-little-wordle': { page: '/tools/dirty-little-wordle', },
      '/tools/namegen': { page: '/tools/namegen', },
      '/words': { page: '/words', },
      '/words/2023-02-19': { page: '/words/2023-02-19', },
      '/words/2023-02-20': { page: '/words/2023-02-20', },
      '/words/2023-02-21': { page: '/words/2023-02-21', },
      '/words/2023-03-01': { page: '/words/2023-03-01', },
      '/words/2023-03-03': { page: '/words/2023-03-03', },
    }),
};

module.exports = nextConfig
